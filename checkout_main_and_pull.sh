#!/bin/bash

for repo in repos/*; do
    [ ! -d "$repo" ] && continue
    echo -e "\e[93m\e[4m\e[1mRepository: $repo\e[39m\e[0m"
    cd "$repo"
    git checkout main || git checkout master
    git pull
    cd ../..
done
