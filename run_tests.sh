#!/bin/bash

set -e

docker-compose --env-file=.env -p zms -f .docker/docker-compose.base.yml run -u 1000:1000 zms bash scripts/run_tests.sh $@
