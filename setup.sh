#!/bin/bash -e

git submodule update --init --recursive
./checkout_main_and_pull.sh
docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml run zms bash scripts/setup.sh
./composer_install.sh
./npm_install.sh
./npm_compile.sh
