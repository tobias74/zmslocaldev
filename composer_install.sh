#!/bin/bash

set -e

args=""
for arg in "$@"; do
    [ "$arg" = "-d" ] && args="$args -f .docker/docker-compose.debug.yml"
    [ "$arg" = "-p" ] && args="$args -f .docker/docker-compose.proxy.yml"
done

docker-compose --env-file=.env -p zms -f .docker/docker-compose.base.yml $args run -u 1000:1000 zms bash scripts/composer_install.sh
