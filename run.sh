#!/bin/bash

set -e

args=""
for arg in "$@"; do
    [ "$arg" = "-d" ] && args="$args -f .docker/docker-compose.debug.yml"
    [ "$arg" = "-p" ] && args="$args -f .docker/docker-compose.proxy.yml"
done


docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml $args up --remove-orphans -d zms
docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml logs -f --tail 100 zms
