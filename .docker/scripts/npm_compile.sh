#!/usr/bin/env bash

set -e

npm cache clean --force

watch_mode=0
repos=()
for arg in "$@"; do
    if [ "$arg" = "-w" ]; then
        watch_mode=1
    else
        repos+=("$arg")
    fi
done

function compile {
    if [[ -f "package.json" ]]; then
        echo -e "\e[93m\e[4m\e[1mRepository: ${PWD##*/}\e[39m\e[0m"
        if [ "$watch_mode" = "1" ]; then
            npm run jswatch --if-present &
            npm run csswatch --if-present &
            npm run reactwatch --if-present &
        else
            npm run js --if-present
            npm run css --if-present
            npm run react --if-present
        fi
    fi
}

if [ ${#repos[@]} -eq 0 ]; then
    for dir in $(find /zmslocaldev/repos -maxdepth 1 -type d); do
        cd $dir
        compile
    done
else
    for repo in "${repos[@]}"; do
        cd "/zmslocaldev/repos/$repo"
        compile
    done
fi

if [ "$watch_mode" = "1" ]; then
    echo -e "\e[93m\e[4m\e[1mWatch mode enabled\e[39m\e[0m"
    while true; do
        sleep 3600
    done
fi
