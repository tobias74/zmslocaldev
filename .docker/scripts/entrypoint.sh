#!/bin/bash

set -e

echo "ZMS LOCAL DEV"

# Migrate Database
/zmslocaldev/repos/zmsapi/vendor/bin/migrate

rm -rf /app/*
mkdir -p /app/terminvereinbarung

cd /zmslocaldev/repos

if [ -d "zmsadmin" ]; then
  echo "zmsadmin setup"
  ln -s /zmslocaldev/repos/zmsadmin/public /app/terminvereinbarung/admin
fi

if [ -d "zmsapi" ]; then
  echo "zmsapi setup"
  mkdir -p /app/terminvereinbarung/api && ln -s /zmslocaldev/repos/zmsapi/public /app/terminvereinbarung/api/2
fi

if [ -d "zmscalldisplay" ]; then
  echo "zmscalldisplay setup"
  ln -s /zmslocaldev/repos/zmscalldisplay/public /app/terminvereinbarung/calldisplay
fi

cd /
supervisord
