#!/usr/bin/env bash

set -e

cd /zmslocaldev/repos
repos=""
[[ $# -eq 0 ]] && repos=* || repos="$@"

for dir in $repos; do
    cd $dir
    if [[ -f "vendor/phpunit/phpunit/phpunit" ]]; then
        echo -e "\e[93m\e[4m\e[1mRepository: $dir\e[39m\e[0m" # fancy colors
        php vendor/phpunit/phpunit/phpunit
    fi
    cd ..
done