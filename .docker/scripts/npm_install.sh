#!/usr/bin/env bash

set -e

mkdir -p "${HOME}/.npm-packages"
npm config set prefix "${HOME}/.npm-packages"

npm cache clean --force

cd /zmslocaldev/repos
for dir in *; do
    cd $dir
    if [[ -f "package.json" ]]; then
        echo -e "\e[93m\e[4m\e[1mRepository: $dir\e[39m\e[0m" # fancy colors
        npm install --include=dev --no-audit --no-fund
    fi
    cd ..
done
