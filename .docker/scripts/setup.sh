#!/bin/bash

for repo in $(find /zmslocaldev/repos -maxdepth 1 -type d); do
  if [ -f "$repo/config.example.php" ] && [ ! -f "$repo/config.php" ]; then
    cp "$repo/config.example.php" "$repo/config.php"
    echo "$repo/config.php created"
  fi
done
