# ZMS Local DEV

# Configuration

## PHP-Version
PHP version can be customized in `.env`. Then simply restart with `run_dev.sh`.

# Setup

## System Requirements
- Tested on Windows, macOS and Linux
- Docker
- Follow instruction "Manage Docker as a non-root user" https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user

## Setup Repositories
For first time setup run `./setup.sh`

# Run
Run ```run.sh```

### Options:
 - `-p` run with proxy, settings defined in `.docker/proxy.env`
 - `-d` run with xdebug, settings defined in `.docker/debug.env`

## Urls
- ZMSAPI Documentation: https://localhost/terminvereinbarung/api/2/doc/index.html
- ZMSAPI example call: https://localhost/terminvereinbarung/api/2/scope/
- ZMSAdmin: https://localhost/terminvereinbarung/admin/
- PHPMyAdmin: http://localhost:8081/

# Scripts

|Script name on host system|Description|
|---|---|
|`./composer_install.sh`|Runs `composer install` on all repositories.|
|`./npm_install.sh`|Runs `npm install` on all repositories with a `package.json`.|
|`./npm_compile.sh`|Compiles frontend assets in all repositories with a `package.json`.|
|`./npm_compile.sh -w`|Compiles frontend assets in all repositories with a `package.json`. Enables watch mode.|
|`./npm_compile.sh -w <MODULE_NAME>`|Compiles frontend assets in the given repository. Enables watch mode.|
|`./reset_containers.sh`|Clears local development environment.|
|`./run_tests.sh`|Runs unit tests in all repositories.|
|`./run_tests.sh zmsadmin zmsapi`|Run unit tests in specified repos.|
|`./dc`|Wrapper script for `docker-compose`, same syntax as `docker-compose`|
