#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

docker-compose --env-file=.env -p zms -f .docker/docker-compose.base.yml down -v
