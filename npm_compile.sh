#!/bin/bash

set -e

dc_args=""
script_args=()
for arg in "$@"; do
    if [ "$arg" = "-d" ]; then
        dc_args="$args -f .docker/docker-compose.debug.yml"
    elif [ "$arg" = "-p" ]; then
        dc_args="$args -f .docker/docker-compose.proxy.yml"
    else
        script_args+=("$arg")
    fi
done

docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml $args run --no-deps node bash scripts/npm_compile.sh ${script_args[@]}
